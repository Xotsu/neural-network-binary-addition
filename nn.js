//https://github.com/AlexDenver/Simple-JS-Neural-Network


class LayerLink{
    constructor(prevNode_count,node_count){
        this.weights = new Matrix(node_count, prevNode_count);
        this.bias = new Matrix(node_count, 1);
        this.weights.randomize();
        this.bias.randomize();
    }
    // //Use this to assign weights from an entity of a generation you want to copy
    // updateWeights(weights){
    //     this.weights = weights;
    // }
    getWeights(){
        return this.weights;
    }
    getBias(){
        return this.bias;
    }
    add(deltaWeight, bias){
        this.weights.add(deltaWeight);
        this.bias.add(bias);
    }
}

class NeuralNetwork {
    constructor(layers) {
        this.activ = {
            activation: function(x){
                return (1 / (1 + Math.exp(-x)) );
            },
            derivative: function(y){
                return (y * (1-y));
            }
        }
        this.learning_rate = 0.1;
        this.layerCount = layers.length - 1;   // Ignoring Output Layer.
        this.inputs = layers[0];
        this.output_nodes = layers[layers.length-1];
        this.layerLink = [];
        for(let i = 1, j = 0; j<(this.layerCount); i++, j++){
          this.layerLink[j] = new LayerLink(layers[j], layers[i]);    // Previous Layer Nodes & Current Layer Nodes
        }

    }

    predict(input_array) {
        let layerResult = Matrix.fromArray(input_array);
        for(let i = 0 ; i < this.layerCount ; i++ ){
            layerResult = Matrix.multiply(this.layerLink[i].getWeights(), layerResult);
            layerResult.add(this.layerLink[i].getBias());
            layerResult.map(this.activ.activation);
        }
        // The Last Layer Result will be the Final Output.
        return layerResult.toArray();
    }

    setLearningRate(n){
      this.learning_rate = n //must be between 0 and 1
    }

    train(input_array, target_array) {
        let layerResult = [Matrix.fromArray(input_array)];
        // Predicting the Result for Given Input, Store Output of each Consequent layer
        for (let i = 0; i < this.layerCount; i++) {
            layerResult[i + 1] = Matrix.multiply(this.layerLink[i].getWeights(), layerResult[i]);
            layerResult[i + 1].add(this.layerLink[i].getBias());
            layerResult[i + 1].map(this.activ.activation);
        }

        let targets = Matrix.fromArray(target_array);
        // Variables to Store Errors and Gradients at each Layer.
        let layerErrors = [];
        let gradients = [];

        // Calculate Actual Error based on Target.
        layerErrors[this.layerCount] = Matrix.subtract(targets, layerResult[this.layerCount]);

        // Correcting and Recalculating Error for each Layer
        for ( let i = this.layerCount ; i >  0 ; i-- ){
            // Calculate the Layer Gradient
            // dyE/dyW = learning_rate * layerError * sigmoid(x) * (1-sigmoid(x));
            // NOTE: dsigmoid = sigmoid(x) * (1-sigmoid(x) ie derivative of sigmoid

            gradients[i] = Matrix.map(layerResult[i], this.activ.derivative);
            gradients[i].multiply(layerErrors[i]);
            gradients[i].multiply(this.learning_rate);

            // Calculate the Changes to be made to the weighs
            let hidden_T = Matrix.transpose(layerResult[i-1]);
            let weight_ho_deltas = Matrix.multiply(gradients[i], hidden_T);

            // Update the Weights and Gradient According to Deltas & Gradient.
            this.layerLink[i-1].add(weight_ho_deltas, gradients[i]);

            // Calculate the Previous Layer Errors (Proportional Error based on Current Layer Error.)
            // NOTE: We are Backpropogating, Therefore we are going backwards 1 step (i.e. i-1)
            layerErrors[i-1] = Matrix.multiply(Matrix.transpose(this.layerLink[i-1].getWeights()), layerErrors[i]);
        }
    }
}
