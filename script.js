//need code which converts given value between 0 and 255 to a binary value
let trainingData = []
let randIntOne
let randIntTwo
let inputs = []
let outputs = []
const trainingSet = 1000000
let testInputs = []
let testOutputs = []
let score = 0
const noTested = 10000
let results
let nn
let currentAnswer = ""
let currentSum = []


function setup(){
  createCanvas(600, 600)
  nn = new NeuralNetwork([16,64,9])
  for (let i = 0; i < trainingSet ; i++){
    randIntOne = randInt()
    randIntTwo = randInt()
    inputs.push(toBin8(randIntOne).concat(toBin8(randIntTwo)))
    outputs.push(toBin9(randIntOne+randIntTwo))
  }
  for (let i = 0; i < trainingSet; i++) {
    nn.train(inputs[i], outputs[i])
  }
  for(let i = 0; i < noTested; i++){
    randIntOne = randInt()
    randIntTwo = randInt()
    testInputs.push(toBin8(randIntOne).concat(toBin8(randIntTwo)))
    testOutputs.push(toBin9(randIntOne+randIntTwo))


      if( testOutputs[i][0] == nn.predict(testInputs[i]).map((x) => Math.round(x) )[0] &&  testOutputs[i][1] == nn.predict(testInputs[i]).map((x) => Math.round(x) )[1] &&  testOutputs[i][2] == nn.predict(testInputs[i]).map((x) => Math.round(x) )[2] &&  testOutputs[i][3] == nn.predict(testInputs[i]).map((x) => Math.round(x) )[3] &&  testOutputs[i][4] == nn.predict(testInputs[i]).map((x) => Math.round(x) )[4] &&  testOutputs[i][5] == nn.predict(testInputs[i]).map((x) => Math.round(x) )[5] &&  testOutputs[i][6] == nn.predict(testInputs[i]).map((x) => Math.round(x) )[6] &&  testOutputs[i][7] == nn.predict(testInputs[i]).map((x) => Math.round(x) )[7] &&  testOutputs[i][8] == nn.predict(testInputs[i]).map((x) => Math.round(x) )[8]){
        score++
      }

  }
  results = (score/noTested)*100

  inputOne = createInput().position(20, 20)
  inputTwo = createInput().position(20, 60)
  processButton = createButton('Add')
  processButton.position(180, 40)
  processButton.mousePressed(addition)
}

function draw(){
  background(200)
  textSize(40)
  fill(255)
  text(results, width/2-100, height/2+50)
  text(currentAnswer, 20, 120)
}

function addition(){
  currentAnswer = ""
  currentSum.push(nn.predict(toBin8(parseInt(inputOne.value(),10)).concat(toBin8(parseInt(inputTwo.value(),10)))).map((x) => Math.round(x) ))
  for (let i = 0; i<9 ; i++){
    currentAnswer += currentSum[0][i].toString()
  }
  currentSum = []
  currentAnswer = parseInt(currentAnswer,2)
}













function toBin8(x){
  let arr = []
  //128, 64, 32, 16, 8, 4, 2, 1,
  //0  ,  0,   0, 0, 0, 0, 0, 0,
  for (i = 0-(8-x.toString(2).length) ; i != x.toString(2).length ; i++ ){
    if(x.toString(2)[i] === undefined){
      arr.push("0")
    }else{
      arr.push(x.toString(2)[i])
    }
  }
  return arr.map(function(i){
    return parseInt(i, 10);
})
}
function toBin9(x){
  let arr = []
  //128, 64, 32, 16, 8, 4, 2, 1,
  //0  ,  0,   0, 0, 0, 0, 0, 0,
  for (i = 0-(9-x.toString(2).length) ; i != x.toString(2).length ; i++ ){
    if(x.toString(2)[i] === undefined){
      arr.push("0")
    }else{
      arr.push(x.toString(2)[i])
    }
  }
  return arr.map(function(i){
    return parseInt(i, 10);
})
}

function randInt(){
  return Math.floor(Math.random()*256)
}
